#ifndef INPUT_MOUSE_H
#define INPUT_MOUSE_H

#include <signal.h>

struct GLFWwindow;

namespace mouse
{

extern volatile sig_atomic_t lmb_press;
extern volatile sig_atomic_t lmb_release;
extern volatile sig_atomic_t lmb_down;

extern volatile sig_atomic_t rmb_press;
extern volatile sig_atomic_t rmb_release;
extern volatile sig_atomic_t rmb_down;

struct Pos
{
	float x, y;
};

Pos
pos(GLFWwindow* window);

void
reset();

} // namespace ms

#endif // INPUT_MOUSE_H
