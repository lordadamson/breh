#include "gl_stuff.h"
#include "glfw_stuff.h"

#include <glad/gl.h>
#include <linmath.h>

#include <stdlib.h>

static const char* vertex_shader_program =
R"(#version 110
uniform mat4 MVP;
attribute vec3 vCol;
attribute vec2 vPos;
varying vec3 color;
void main()
{
gl_Position = MVP * vec4(vPos, 0.0, 1.0);
color = vCol;
})";

static const char* fragment_shader_program =
R"(#version 110
varying vec3 color;
void main()
{
gl_FragColor = vec4(color, 1.0);
})";

void
gl::init(Context& ctx)
{
	GLuint vertex_buffer, vertex_shader, fragment_shader, program,
		mvp_location, vpos_location, vcol_location;

	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

	glBufferData(GL_ARRAY_BUFFER,
				 GLuint(sizeof(ctx.box.vertices[0]) * ctx.box.vertices_count),
				 ctx.box.vertices, GL_STATIC_DRAW);

	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_shader_program, nullptr);
	glCompileShader(vertex_shader);

	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_shader_program, nullptr);
	glCompileShader(fragment_shader);

	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);

	mvp_location = GLuint(glGetUniformLocation(program, "MVP"));
	vpos_location = GLuint(glGetAttribLocation(program, "vPos"));
	vcol_location = GLuint(glGetAttribLocation(program, "vCol"));

	glEnableVertexAttribArray(vpos_location);
	glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
						  sizeof(ctx.box.vertices[0]), nullptr);

	glEnableVertexAttribArray(vcol_location);
	glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
						  sizeof(ctx.box.vertices[0]), (void*)(sizeof(float) * 2));

	ctx.program = program;
	ctx.box.mvp_location = mvp_location;
}

void
gl::update(gl::Context& ctx)
{
	float ratio;
	int width, height;
	mat4x4 p, mvp;
	glfw::fb_size(ctx.window, width, height);
	ratio = width / (float) height;
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);

	mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);

	mat4x4_mul(mvp, p, ctx.box.mat);
	glUseProgram(ctx.program);
	glUniformMatrix4fv(ctx.box.mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
	glDrawArrays(GL_TRIANGLES, 0, ctx.box.vertices_count);
}
