#ifndef GLFW_STUFF_H
#define GLFW_STUFF_H

struct GLFWwindow;

namespace glfw
{

GLFWwindow*
init();

void
update(GLFWwindow*);

void
destroy(GLFWwindow*);

bool
should_exit(GLFWwindow*);

void
fb_size(GLFWwindow*, int& width, int& height);

} // glfw


#endif // GLFW_STUFF_H
