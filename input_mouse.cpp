#include "input_mouse.h"

#include <GLFW/glfw3.h>

namespace mouse
{

volatile sig_atomic_t lmb_press;
volatile sig_atomic_t lmb_release;
volatile sig_atomic_t lmb_down;

volatile sig_atomic_t rmb_press;
volatile sig_atomic_t rmb_release;
volatile sig_atomic_t rmb_down;

Pos
pos(GLFWwindow* window)
{
	double x, y;
	glfwGetCursorPos(window, &x, &y);
	return Pos{float(x), float(y)};
}

void
reset()
{
	lmb_press  = false;
	lmb_release = false;
	rmb_press  = false;
	rmb_release = false;
}

} // namespace mouse
