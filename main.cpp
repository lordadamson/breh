#include "gl_stuff.h"
#include "glfw_stuff.h"
#include "input_mouse.h"

#include <stdint.h>
#include <stdio.h>

int main(void)
{
	GLFWwindow* window = glfw::init();

	gl::Context ctx{
		window,
		widget_init(window),
		0
	};

	gl::init(ctx);

	while (!glfw::should_exit(window))
	{
		glfw::update(window);
		widget_update(ctx.box);
		gl::update(ctx);
	}

	glfw::destroy(window);
	widget_destroy(ctx.box);
	return 0;
}
