#include "glfw_stuff.h"
#include "input_mouse.h"

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>

static void
_on_error(int, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

static void
_on_key_event(GLFWwindow* window, int key, int, int action, int)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

static void
poll_mouse_event(GLFWwindow* window)
{
	mouse::reset();

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		if(mouse::lmb_down)
		{
			mouse::lmb_press = false;
			return;
		}

		mouse::lmb_press = true;
		mouse::lmb_down = true;
	}

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	{
		if(mouse::lmb_down == false)
		{
			mouse::lmb_release = false;
			return;
		}

		mouse::lmb_release = true;
		mouse::lmb_down = false;
	}

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		if(mouse::rmb_down)
		{
			mouse::rmb_press = false;
			return;
		}

		mouse::rmb_press = true;
		mouse::rmb_down = true;
	}

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE)
	{
		if(mouse::rmb_down == false)
		{
			mouse::rmb_release = false;
			return;
		}

		mouse::rmb_release = true;
		mouse::rmb_down = false;
	}
}

GLFWwindow*
glfw::init()
{
	glfwSetErrorCallback(_on_error);

	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	GLFWmonitor* monitor = glfwGetPrimaryMonitor();

	int width, height, _;
	glfwGetMonitorWorkarea(monitor, &_, &_, &width, &height);

	GLFWwindow* window = glfwCreateWindow(width, height, "Breh", nullptr, nullptr);

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, _on_key_event);

	glfwMakeContextCurrent(window);
	gladLoadGL();
	glfwSwapInterval(1);

	return window;
}

void
glfw::update(GLFWwindow* window)
{
	glfwSwapBuffers(window);
	glfwPollEvents();
	poll_mouse_event(window);
}

void
glfw::destroy(GLFWwindow* window)
{
	glfwDestroyWindow(window);
	glfwTerminate();
}

bool
glfw::should_exit(GLFWwindow* window)
{
	return glfwWindowShouldClose(window);
}

void
glfw::fb_size(GLFWwindow* window, int& width, int& height)
{
	glfwGetFramebufferSize(window, &width, &height);
}
