#include "widget.h"
#include "gl_stuff.h"
#include "glfw_stuff.h"
#include "input_mouse.h"
#include "defer.h"

#include <stdio.h>
#include <string.h>

static Vertex vertices[] =
{
	{-0.2f,  0.2f, .8f, 1.0f, 1.0f}, // Top-left
	{0.2f,  0.2f,  .8f, 1.0f, 1.0f}, // Top-right
	{0.2f, -0.2f,  .8f, 1.0f, 1.0f}, // Bottom-right

	{0.2f, -0.2f,  .8f, 1.0f, 1.0f}, // Bottom-right
	{-0.2f, -0.2f, .8f, 1.0f, 1.0f}, // Bottom-left
	{-0.2f,  0.2f, .8f, 1.0f, 1.0f}  // Top-left
};

static const float original_bb_tl[2] = {-0.2f,  0.2f};
static float bb[4] = {
	-0.2f,  0.2f, // Top-left
	0.4f, 0.4f   // width and height
};

struct WidgetPrivate
{
	mouse::Pos prev_mouse_pos;
	float old_mouse_pos[2];
	bool dragging = false;
};

static bool
is_in_bb(float mouse_pos[2], float bb[4])
{
	if(mouse_pos[0] < bb[0])
	{
		return false;
	}

	if(mouse_pos[0] > bb[0] + bb[2])
	{
		return false;
	}

	if(mouse_pos[1] > bb[1])
	{
		return false;
	}

	if(mouse_pos[1] < bb[1] - bb[3])
	{
		return false;
	}

	return true;
}

Widget
widget_init(GLFWwindow* window)
{
	Widget box{};

	box.window = window;
	box.vertices = vertices;
	box.vertices_count = sizeof(vertices)/sizeof(vertices[0]);
	mat4x4_identity(box.mat);

	box.impl = static_cast<WidgetPrivate*>(calloc(sizeof(WidgetPrivate), 1));
	box.impl->old_mouse_pos[0] = 512.0f;
	box.impl->old_mouse_pos[1] = 512.0f;

	return box;
}

void
widget_update(Widget& self)
{
	int width, height;
	glfw::fb_size(self.window, width, height);

	mouse::Pos cur_pos = mouse::pos(self.window);

	if(cur_pos.x < 0)
	{
		return;
	}

	if(cur_pos.y < 0)
	{
		return;
	}

	if(cur_pos.x > width)
	{
		return;
	}

	if(cur_pos.y > height)
	{
		return;
	}

	float x = (2.0f * cur_pos.x) / width - 1.0f;
	float y = 1.0f - (2.0f * cur_pos.y) / height;

	vec4 ray_clip = {x, y, -1.0f, 1.0f};

	mat4x4 projection, inverse_projection;

	float ratio = width / float(height);
	mat4x4_ortho(projection, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
	mat4x4_invert(inverse_projection, projection);

	vec4 ray_eye;
	mat4x4_mul_vec4(ray_eye, inverse_projection, ray_clip);
	ray_eye[2] = -1.0f;
	ray_eye[3] = 0.0f;

	mat4x4 inverse_model;
	mat4x4_invert(inverse_model, self.mat);

	vec4 ray_m;
	mat4x4_mul_vec4(ray_m, inverse_model, ray_eye);

	float mouse_pos[2] = {ray_m[0], ray_m[1]};
	mn_defer( memcpy(self.impl->old_mouse_pos, mouse_pos, sizeof(mouse_pos)) );

	if(!self.impl->dragging)
	{
		if(!mouse::lmb_press)
		{
			return;
		}

		if(!is_in_bb(mouse_pos, bb))
		{
			return;
		}

		self.impl->dragging = true;
	}

	if(mouse::lmb_release)
	{
		self.impl->dragging = false;
		return;
	}

	if(self.impl->old_mouse_pos[0] == 512.0f)
	{
		self.impl->old_mouse_pos[0] = mouse_pos[0];
		self.impl->old_mouse_pos[1] = mouse_pos[1];
	}

	float offsetx = mouse_pos[0] - self.impl->old_mouse_pos[0];
	float offsety = mouse_pos[1] - self.impl->old_mouse_pos[1];

	self.mat[3][0] += offsetx;
	self.mat[3][1] += offsety;

	bb[0] = original_bb_tl[0] + self.mat[3][0];
	bb[1] = original_bb_tl[1] + self.mat[3][1];
}

void
widget_destroy(Widget& self)
{
	free(self.impl);
}
