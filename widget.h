#ifndef WIDGET_H
#define WIDGET_H

#include <stddef.h>

#include <linmath.h>

struct Vertex
{
	float x, y, r, g, b;
};

struct WidgetPrivate;
struct GLFWwindow;

struct Widget
{
	typedef unsigned int GLint;
	typedef int GLsizei;

	GLFWwindow* window;
	Vertex* vertices;
	GLsizei vertices_count;
	GLint mvp_location;
	mat4x4 mat;

	WidgetPrivate* impl;
};

Widget
widget_init(GLFWwindow*);

void
widget_update(Widget& self);

void
widget_destroy(Widget& self);

#endif // WIDGET_H
