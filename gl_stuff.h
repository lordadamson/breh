#ifndef GL_STUFF_H
#define GL_STUFF_H

#include "widget.h"

#include <stdint.h>

struct GLFWwindow;

namespace gl
{

struct Context
{
	typedef unsigned int GLuint;

	GLFWwindow* window;
	Widget box;
	GLuint program;
};

void
init(Context&);

void
update(Context&);

} // namespace gl

#endif // GL_STUFF_H
